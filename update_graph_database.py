__author__ = 'justingibbons'

from py2neo import Graph, Node,Relationship

class NonUniqueError(Exception):
    def __init__(self,value):
        self.value=value
    def __str__(self):
        return repr(self.value)

class NothingFoundError(Exception):
    def __init__(self,value):
        self.value=value
    def __str__(self):
        return repr(self.value)


class RelationshipAlreadyExists(Exception):
    def __init__(self,value):
        self.value=value
    def __str__(self):
        return repr(self.value)

class Update_Graph:
    def __init__(self,graph):
        """Class contains methods for updating the supplied graph with information that is feed to it"""
        self.graph=graph

    def __str__(self):
        return repr(self.value)


    def node_exists(self,label,unique_constraint_property,unique_constraint_value):
        """Returns true if the node with the given label has the given unique_constraint_value for the
        unique_constraint_property"""
        if len(list(self.graph.find(label,unique_constraint_property,unique_constraint_value)))>0:
            return True
        else:
            return False

    def relationship_exists(self,start_node,end_node,relationship_type):
        """Returns true if a relationship exists between start_node and end_node of relationship type
        and false otherwise"""
        if len(list(self.graph.match(start_node=start_node,end_node=end_node,rel_type=relationship_type))) > 0:
            return True
        else:
            return False


    def add_nodes(self,node_list,unique_constraint=None,unique_constraint_label=None):
        """Uses the cast method to add a list of nodes to graph. If unique_constraint doesn't equal None
        than will not create a new node if a node with that value already exists. Note """

        #May want to rewrite with methods in Legacy. Though I am not sure why they are in something called legacy
        for data in node_list:
            if unique_constraint==None: #If no constraint is applied add the node no matter what
                node=Node.cast(data)
                self.graph.create(node)
                node.push()
            else: #Otherwise only add the node if the unique constraint value isn't already in the database
                property_value=data[1][unique_constraint] #get the value of the unique constraint

                if self.node_exists(unique_constraint_label,unique_constraint,property_value):
                    continue
                else:
                    node=Node.cast(data)
                    self.graph.create(node)
                    node.push()


    def batch_add_nodes(self,node_list):
        """First checks to see if the nodes are unique. Uses the create method to add the unique nodes and returns
        the nodes as a list that were found to already exist in the graph"""
        pass

    def get_node(self,search_criteria_dic):
        """Takes a dictionary in the form {"label":node1_label,"property":p1, "value":v1} where "label" is the
        type of node to search for, "property" is the property type that is to be searched for and value is the
         value of that property. If only one node is found it returns it otherwise an exception is thrown indicating
         that your search criteria did not find a unique node (including finding no nodes"""

        #Dictionary keys
        label_key="label"
        property_key="property"
        value_key="value"
        label=search_criteria_dic[label_key]
        property=search_criteria_dic[property_key]
        value=search_criteria_dic[value_key]
        node=list(self.graph.find(label,property,value))

        if len(node)>1:
            raise NonUniqueError("Search criteria returned more than one node")
        elif len(node)==1:
            return node[0]
        else:
            raise NothingFoundError("Node with value "+str(value)+" not found")




    def add_relationship_between_existing_nodes(self,node_pairs_dic):
        """Takes a dictionary of the form:
            {"node1": {"label":node1_label,"property":p1, "value":v1},
            "node2": {"label":node2_label,"property":p2, "value":v2},
            "bidirectional":bool,
            "relationship_properties:{"property1":pv1,"property2":pv2...},
            "relationship_labels":label_value}
            Where node1 and node2 indicate the nodes that are to be connected. The "label" key indicates which nodes to
            search and"property key indicates the constraint that is to be used to identify the node and the "value" key
             indicates the value of the constraint. If the "bidirectional" key points to False then the relationship is
             drawn from node1 to node2. The "relationship_properties" key points to a dictionary containing the property attributes
             the relationship is to have. The "relationship_labels" key points to the label for the relationship.
             Will throw an exception if the relationship already exists"""

        node1_key="node1"
        node2_key="node2"
        label_key="label"
        property_key="property"
        value_key="value"
        bidirectional_key="bidirectional"
        relationship_properties_key="relationship_properties"
        relationship_labels_key="relationship_labels"

        #Find the nodes

        #Get node1
        node1_label=node_pairs_dic[node1_key][label_key]
        node1_property=node_pairs_dic[node1_key][property_key]
        node1_property_value=node_pairs_dic[node1_key][value_key]
        node1=self.get_node({label_key:node1_label,property_key:node1_property,value_key:node1_property_value})

        #Get node2
        node2_label=node_pairs_dic[node2_key][label_key]
        node2_property=node_pairs_dic[node2_key][property_key]
        node2_property_value=node_pairs_dic[node2_key][value_key]
        node2=self.get_node({label_key:node2_label,property_key:node2_property,value_key:node2_property_value})

        #Write the relationships

        relationship_label=node_pairs_dic[relationship_labels_key]
        bidirectional_bool=node_pairs_dic[bidirectional_key]
        relationship_tup=(node_pairs_dic[relationship_labels_key],node_pairs_dic[relationship_properties_key])

        #Check to see if the relatioship exists first
        if self.relationship_exists(node1,node2,relationship_label):
            #If the relationship exits throw exception
            raise RelationshipAlreadyExists(str(node1_property_value)+" and "+ str(node2_property_value)+ " already have share the relationship "+relationship_label)
        else:
            relationship=Relationship.cast(node1, relationship_tup, node2)
            self.graph.create(relationship)

            relationship.push()

        #If the relationship is bidirectional add the relationship going the other way if it doesn't already exist
        if bidirectional_bool:
            if self.relationship_exists(node2,node1,relationship_label):
                raise RelationshipAlreadyExists(str(node2_property_value)+" and "+ str(node1_property_value)+ " already have share the relationship "+relationship_label)
            else:
                relationship=Relationship(node2,relationship_tup,node1)
                self.graph.create(relationship)
                relationship.push()





    def add_list_of_relationships_between_existing_nodes(self,list_of_relationships):
        """Applies add_relationship_between_existing_nodes method to every dictionary in a list"""
        for relationship_dic in list_of_relationships:
            self.add_relationship_between_existing_nodes(relationship_dic)


if __name__=="__main__":
    from py2neo import authenticate
    import unittest,subprocess

    # authenticate("localhost:7474","neo4j","genome")
    # graph=Graph("http://localhost:7474/db/data")
    # node_list=[("Person",{"name":"Alice","birth_year":1960}),("Person",{"name":"Alice"}),
    #                     ("Person",{"name":"Charlie"})]
    # update_graph=Update_Graph(graph)
    # update_graph.add_nodes(node_list,unique_constraint="name",unique_constraint_label="Person")
    # print list(graph.find("Person"))


    # class Test_Update_Graph_Node_Creation_Methods(unittest.TestCase):
    #     """"Get a socket error if try to run all tests. If need to test a function uncomment the unittest for it"""
    #
    #     def setUp(self):
    #         authenticate("localhost:7474","neo4j","genome")
    #         self.graph=Graph("http://localhost:7474/db/data")
    #         self.update_graph=Update_Graph(self.graph)
    #
    #
    #     # def tearDown(self):
    #     #      #Stop the server
    #     #     subprocess.call("~/neo4j-community-2.3.0/bin/neo4j stop",shell=True)
    #     #     #Delete the database
    #     #     subprocess.call("rm -rf ~/neo4j-community-2.3.0/practice_database",shell=True)
    #     #     #Restart the database
    #     #     subprocess.call("~/neo4j-community-2.3.0/bin/neo4j start",shell=True)
    #
    #
    #
    #
    #
    #     # def test_node_exists(self):
    #     #     node_list=[("Person",{"name":"Alice"})]
    #     #     update_graph=Update_Graph(self.graph)
    #     #     #Rewrite test so out come is not dependent on the add_nodes method
    #     #     update_graph.add_nodes(node_list,unique_constraint="name",unique_constraint_label="Person")
    #     #     self.assertEqual(update_graph.node_exists("Person","name","Alice"),True)
    #     #     self.assertEqual(update_graph.node_exists("Person","name","Charlie"),False)
    #     #
    #     # def test_add_nodes(self):
    #     #     node_list=[("Person",{"name":"Alice","birth_year":1960}),("Person",{"name":"Alice"}),
    #     #                ("Person",{"name":"Charlie","nicknames":["Charles","Chuck"]})]
    #     #     correct_answer="[<Node graph=u'http://localhost:7474/db/data/' ref=u'node/0' labels=set([u'Person']) properties={u'name': u'Alice', u'birth_year': 1960}>, <Node graph=u'http://localhost:7474/db/data/' ref=u'node/1' labels=set([u'Person']) properties={u'name': u'Charlie', u'nicknames': [u'Charles', u'Chuck']}>]"
    #     #     update_graph=Update_Graph(self.graph)
    #     #     update_graph.add_nodes(node_list,unique_constraint="name",unique_constraint_label="Person")
    #     #     self.assertEqual(str(list(self.graph.find("Person"))),correct_answer)
    #
    #     def test_add_relationship_between_existing_nodes(self):
    #
    #         relationship_dict={"node1": {"label":"Person","property":"name", "value":"Alice"},
    #         "node2": {"label":"Person","property":"name", "value":"Charlie"},
    #         "bidirectional":False,
    #         "relationship_properties":{"since":1987},
    #         "relationship_labels":set(["Knows","Friends"])}
    #         self.update_graph.add_relationship_between_existing_nodes(relationship_dict)
    #

    # class Test_Update_Graph_Relationship_Creation_Methods(unittest.TestCase):
    #     def setUp(self):
    #         authenticate("localhost:7474","neo4j","genome")
    #         self.graph=Graph("http://localhost:7474/db/data")
    #         self.update_graph=Update_Graph(self.graph)
    #
    #         self.alice=Node("Person", name="Alice",birthday=1960)
    #         self.charlie=Node("Person",name="Charlie",birthday=1960)
    #         self.james=Node("Person", name="James",birthday=1985)
    #     #
    #         self.graph.create(self.alice)
    #         self.graph.create(self.charlie)
    #         self.graph.create(self.james)
    #     # #
    #         self.alice.push()
    #         self.charlie.push()
    #         self.james.push()
    #
    #         self.relationship_dict1={"node1": {"label":"Person","property":"name", "value":"Alice"},
    #         "node2": {"label":"Person","property":"name", "value":"Charlie"},
    #         "bidirectional":True,
    #         "relationship_properties":{"since":1987},
    #         "relationship_labels":"Knows"}
    #
    #         self.relationship_dict2={"node1": {"label":"Person","property":"name", "value":"Alice"},
    #         "node2": {"label":"Person","property":"name", "value":"James"},
    #         "bidirectional":False,
    #         "relationship_properties":{"since":2005},
    #         "relationship_labels":"Knows"}
    #
    #         self.relationship_list=[self.relationship_dict1,self.relationship_dict2]



        # def tearDown(self):
        #      #Stop the server
        #     subprocess.call("~/neo4j-community-2.3.0/bin/neo4j stop",shell=True)
        #     #Delete the database
        #     subprocess.call("rm -rf ~/neo4j-community-2.3.0/practice_database",shell=True)
        #     #Restart the database
        #     subprocess.call("~/neo4j-community-2.3.0/bin/neo4j start",shell=True)

        # def test_add_relationship_between_existing_nodes(self):
        #
        #     #This test is dependent on relationhip_exists method working corretly (so not a good test)
        #     self.update_graph.add_relationship_between_existing_nodes(self.relationship_dict1)
        #     self.assertEqual(self.update_graph.relationship_exists(self.alice,self.charlie,"Knows"),True)
        #     self.assertEqual(self.update_graph.relationship_exists(self.charlie,self.alice,"Knows"),True)
        #
        #     #Test the exception
        #     with self.assertRaises(RelationshipAlreadyExists):
        #         self.update_graph.add_relationship_between_existing_nodes(self.relationship_dict1)

        # def test_add_list_of_relationships_between_existing_nodes(self):
        #     self.update_graph.add_list_of_relationships_between_existing_nodes(self.relationship_list)






    class Test_Graph_Query_Methods(unittest.TestCase):

        def setUp(self):
            authenticate("localhost:7474","neo4j","genome")
            self.graph=Graph("http://localhost:7474/db/data")
            self.update_graph=Update_Graph(self.graph)
            self.alice=Node("Person", name="Alice",birthday=1960)
            self.charlie=Node("Person",name="Charlie",birthday=1960)
            self.relationship=Relationship(self.alice, "Knows", self.charlie)

            self.graph.create(self.alice)
            self.graph.create(self.charlie)
            self.graph.create(self.relationship)


        # def tearDown(self):
        #      #Stop the server
        #     subprocess.call("~/neo4j-community-2.3.0/bin/neo4j stop",shell=True)
        #     #Delete the database
        #     subprocess.call("rm -rf ~/neo4j-community-2.3.0/practice_database",shell=True)
        #     #Restart the database
        #     subprocess.call("~/neo4j-community-2.3.0/bin/neo4j start",shell=True)


        def test_get_node(self):
            correct_answer1="Charlie"
           #Check to see if its returning the correct name
            self.assertEqual(self.update_graph.get_node({"label":"Person","property":"name", "value":"Charlie"})["name"],correct_answer1)
            #Make sure correct exceptions are being raised
            with self.assertRaises(NonUniqueError):
                self.update_graph.get_node({"label":"Person","property":"birthday", "value":1960})

            with self.assertRaises(NothingFoundError):
                self.update_graph.get_node({"label":"Person","property":"name", "value":"Chuck"})

        def test_relationship_exists(self):
            self.assertEqual(self.update_graph.relationship_exists(self.alice,self.charlie,"Knows"),True)
            self.assertEqual(self.update_graph.relationship_exists(self.alice,self.charlie,"Friends"),False)




    unittest.main()
